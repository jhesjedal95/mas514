===================
Guides
===================

******************************
Install RealSense2 - ROS
******************************

The installation steps are based on: https://github.com/IntelRealSense/librealsense/blob/master/doc/installation_jetson.md 

First update:

   * ``sudo apt-get update``
   * ``sudo apt-get upgrade``

1. Install ROS Distrobutions: 

   * ``sudo apt-get install ros-$ROS_DISTRO-realsense2-camera``
   * ``sudo apt-get install ros-$ROS_DISTRO-realsense2-description``    

2. Register the server's public key by writing the following command in the Jetbot Terminal:  
   
   * ``sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-key F6E65AC044F831AC80A06380C8B3A55A6F3EFCDE || sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-key F6E65AC044F831AC80A06380C8B3A55A6F3EFCDE`` 

3.	Add the server to the list of repositories: 

   * ``sudo add-apt-repository "deb https://librealsense.intel.com/Debian/apt-repo $(lsb_release -cs) main" -u``

4.  Install the SDK:
   
    * ``sudo apt-get install librealsense2-utils``

    if you got this error related to "sub-process /usr/bin/dpkg returned an error code (1)" execute these commands:

    * ``sudo rm /var/lib/dpkg/info/<<write the package name>>*``     note: keep this mark *
    * ``sudo dpkg --configure -D 777 <<write the package name>>``
    * ``sudo apt -f install``

5. Execute this command:
   
   * ``sudo apt-get install librealsense2-dev``

6. Finally to verify your installation execute this command in your terminal on the Jetson Nano, but first you to connect the Jetbot to a screen, keyboard, and mouse:   

   * ``roslaunch realsense2_camera demo_pointcloud.launch``

   * This start both roscore and rviz , and you should be able to see the stream    

.. figure:: ../figs/L515/PointCloud2_Rviz.png
    :width: 500px
    :align: center

   * Then down to the left, you can click Add, then add by topic and you can show other camera modes 

7. Install Intel® RealSense™ ROS from Sources

   * navigate to  ``cd ~/catkin_ws/src/``
   * ``git clone https://github.com/IntelRealSense/realsense-ros.git``
   * ``cd realsense-ros/``
   * ``git checkout `git tag | sort -V | grep -P "^2.\d+\.\d+" | tail -1```
   * ``cd ..``
   * ``catkin_init_workspace`` 
   * ``cd ..`` 
   * ``catkin_make clean`` 
   * ``catkin_make -DCATKIN_ENABLE_TESTING=False -DCMAKE_BUILD_TYPE=Release -DBUILD_WITH_CUDA=true`` 
   * ``echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc`` 
   * ``source ~/.bashrc`` 
   * ``roslaunch realsense2_camera rs_camera.launch`` 
   * I you get error related to .... replace the ``librealsense_camera.so`` file located in ``home/catkin_ws/devel/lib`` folder with the file located in ``opt/ros/melodic/lib`` and try again
   * Modify the ``rs_camera2.launch`` file: set true on enable_pointcloud in line 46
   * Test if the pointcloud is working
      * ``roslaunch realsense2_camera rs_camera.launch`` 
      * ``rviz`` 
      * Add poitcloud 2, and select topic

******************************
PointClound to LaserScan
******************************

Link to the ros wiki: http://wiki.ros.org/pointcloud_to_laserscan 

1. Install from apt:
   
   * ``sudo apt-get install ros-melodic-pointcloud-to-laserscan``

2. Navigate to: 
   
   * ``cd catkin-ws/src``

3. Clone Git repo (branch: lunar-devel):   
    
   * ``git clone -b lunar-devel https://github.com/ros-perception/pointcloud_to_laserscan``

4. Navigate to catkin_ws: 
   
   * ``cd ..``

5. Build code: 
   
   * ``catkin_make``

6. Modify sample_node.launch file to look like this:

.. literalinclude:: ../../Arduino/ROS_test/sample_node.launch
    :language: c++   

7. To test it, (if not already running) start the real sense pointcloud demo from the terminal on the Jetson Nano:  
   
   * ``roslaunch realsense2_camera demo_pointcloud.launch``

    Deselect the Pointcloud2 stream.

    Then, in a new terminal on the Jetson Nano:

    * ``roslaunch pointcloud_to_laserscan sample_node.launch``   

    In Rviz, click "Add" and chose "Lase Scan." Then select the available topic from the dropdown list. Shpuld be "camera/scan" as defined in the launchfile. 

    Now you should see the laser scan.

8. Play around with the parameters in the launch file as described here: http://wiki.ros.org/pointcloud_to_laserscan. Take a look at the L515 manual available here: https://dev.intelrealsense.com/docs/lidar-camera-l515-datasheet to get proper parameters (max and min distance, etc) 


******************************
How to Publish Wheel Encoder Data Using ROS and Arduino
******************************
The following guides are inspired by: https://automaticaddison.com/how-to-publish-wheel-encoder-tick-data-using-ros-and-arduino/

------------------
Install rosserial_arduino
------------------

1. First download and install Arduino IDE on the Jetson Nano (Linux ARM 64 bits): https://downloads.arduino.cc/arduino-1.8.16-linuxaarch64.tar.xz

2. Navigate to the Downlods folder and extract the .xz file

3. When the extracting is complete, open terminal an navigate to the folder unziped folder: 

   * ``cd ~/Downloads/arduino-1.8.16-linuxaarch64/arduino-1.8.16``

4. Execute the following in the terminal ``sudo ./install.sh``

5. Test to Upload code to the Arduino Nano:

   * Connect the Arduino Nano to the Jetson Nano.   
   * Open the Arduino IDE by clicking on the file on the desktop.
   * Under "Tools" select correct board (Arduino Nano) and Port (/dev/ttyUSB0).
   * Enable serial permission, open terminal and type: 
      * ``sudo usermod -a -G dialout jetbot``  
      * You will need to ``sudo reboot`` for this change to take effect.
   * Under "Tools" select correct board (Arduino Nano) and Port (/dev/ttyUSB0).   
   * Varify and Uppload the initial scetch. 
   * Any errors? If not contintue.

6. Install rosserial_arduino, by open new terminal end execute the following: 

   * ``sudo apt-get install ros-melodic-rosserial-arduino``
   * ``sudo apt-get install ros-melodic-rosserial``
  
7. Install ros_lib into the Arduino Environment
   
   *  ``cd Arduino/libraries``
   *  ``rm -rf ros_lib``
   *  ``rosrun rosserial_arduino make_libraries.py .``

8. In the Arduino IDE, under "File" navigate to "Examples" --> "ros_lib" and select Odom

9. Verify and Upload the Scetch

10. Running the Odom Example in ROS (for other examples checkout: http://wiki.ros.org/rosserial_arduino/Tutorials) 
    
    * Open new terminal and execute: ``roscore``
    * Open new terminal and execute: ``rosrun rosserial_python serial_node.py /dev/ttyUSB0``
    * Open new terminal and execute: ``rviz``
    * In Rviz change the "Fixed Frame" from "map" to "odom"
    * Click "Add" and add TF and the simulation running on the Arduino should be visable - rotating the base_link arround the fixed frame "odom"

   
------------------
Encoder Signal Publisher Node
------------------

The Arduino scetch from LAB exercise #3 - problem 1c may be converted to a ROS node as shown in the code below:

.. literalinclude:: ../../Arduino/encoder_ros/encoder_ros.ino
    :language: c++  

1. Copy the code into a new scetch, click on verify, and then uppload the code to the Arudino Nano. 
   
2. Connect the Arduino to the Jetson Nano board over USB and connect the encoder PWM signal from both motors and connect power (if you don't alread have cables for connecting power to the Arduino skip it since the board already gets power over USB) as shown in the picture below: 

.. figure:: ../figs/labexercise/Ex3_Problem1a_2.png
    :width: 500px
    :align: center

3. Open a new terminal and execute: ``roscore``

4. Open a new terminal and execute: ``rosrun rosserial_arduino serial_node.py _port:=/dev/ttyUSB0 _baud:=115200``
   
5. Open a new terminal and check if there is a topic for left and right wheel angle: ``rostopic list``
   
6. To read the encoder angles excute the following command in different terminals: ``rostopic echo /angle_left_wheel`` and ``rostopic echo /angle_right_wheel``

7. Rotate the wheels of the JetBot and verify that the encoder value (degrees) are changing. 

******************************
How to Publish Odometry using Wheel Encoder Position
******************************
The following guides are inspired by: https://gist.github.com/atotto/f2754f75bedb6ea56e3e0264ec405dcf and the topics sendt from the Arduino is subsribed.

------------------
node
------------------

.. literalinclude:: ../../Arduino/ROS_test/Odometry.py
    :language: python
------------------
launch file
------------------

.. literalinclude:: ../../Arduino/ROS_test/start.launch
    :language: launch
