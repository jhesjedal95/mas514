/*
* MAS514
* Ex4 - Problem 1b Solution
*/

// Include library of the IMU (MPU-9250)
#include "mpu9250.h"
// Declear Variables
float AccX,AccY,AccZ,GyroX,GyroY,GyroZ,MagX,MagY,MagZ,Temp;
float dt,t,t_1;
float GyroRoll,GyroPitch,GyroYaw;
float yaw,pitch,roll;
float GyroErrorX, GyroErrorY, GyroErrorZ;

/* An Mpu9250 object with the MPU-9250 sensor on I2C bus 0 with address 0x68 */
Mpu9250 imu(&Wire, 0x68);
int status;

void setup() {
  /* Serial to display data */
  Serial.begin(19200);
  while(!Serial) {}
  /* Start communication */
  if (!imu.Begin()) {
    Serial.println("IMU initialization unsuccessful");
    while(1) {}
  }
}

void loop() {
  /* Read the sensor */
  if (imu.Read()) {
    
    // Accelerometer [m/s^2]
    AccX = imu.accel_x_mps2();   
    AccY = imu.accel_y_mps2();   
    AccZ = imu.accel_z_mps2();  
    // Gyroscope [deg/s]
    GyroX = imu.gyro_x_radps()*(180/PI);   
    GyroY = imu.gyro_y_radps()*(180/PI);    
    GyroZ = -imu.gyro_z_radps()*(180/PI);   
    // Magnometer [micro Tesla]
    MagX = imu.mag_x_ut();  
    MagY = imu.mag_y_ut();    
    MagZ = imu.mag_z_ut();    
    // Temperature [deg C]
    Temp = imu.die_temperature_c(); 

    // Problem 1d - Correct the outputs with the calculated error values
    GyroErrorX = 2.26;
    GyroErrorY = 2.32;
    GyroErrorZ = 0.24;
    GyroX = GyroX - GyroErrorX; 
    GyroY = GyroY - GyroErrorY; 
    GyroZ = GyroZ - GyroErrorZ; 

    // Problem 1b - Estimate yaw, pitch, and roll by integrating the measured angular velocity of the gyroscope
    t_1 = t;                 // Previous time is stored before the actual time read
    t = millis();            // Current time actual time read
    dt = (t - t_1) / 1000;   // Divide by 1000 to get seconds    

    GyroYaw = GyroYaw + GyroZ*dt;
    GyroPitch = GyroPitch + GyroY*dt;
    GyroRoll = GyroRoll + GyroX*dt;

    yaw = GyroYaw;
    pitch = GyroPitch;
    roll = GyroRoll;

    // Serial Plot 
   /* 
    // Accelerometer
    Serial.print(AccX, 2);
    Serial.print(",");
    Serial.print(AccY, 2);
    Serial.print(",");
    Serial.print(AccZ, 2);
    Serial.print(",");
    // Gyroscope
    Serial.print(GyroX, 2);
    Serial.print(",");
    Serial.print(GyroY, 2);
    Serial.print(",");
    Serial.print(GyroZ, 2);
    Serial.print(",");
    // Magnometer
    Serial.print(MagX, 2);
    Serial.print(",");
    Serial.print(MagY, 2);
    Serial.print(",");
    Serial.print(MagZ, 2);
    Serial.print(",");
    // Temperatur
    Serial.print(Temp, 1);
    Serial.print(",");
    // yaw, pitch, roll
    Serial.print(GyroYaw);
    Serial.print(",");
    Serial.print(GyroPitch);
    Serial.print(",");
    Serial.print(GyroRoll);  
    Serial.print(",");
    */
    Serial.print(yaw);
    Serial.print(",");
    Serial.print(pitch);
    Serial.print(",");
    Serial.println(roll);  
  }
}
