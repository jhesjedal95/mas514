/*
  *MAS514
  * Problem 1d - Calculate error for Calibration
*/

#include "mpu9250.h"

float GyroX, GyroY, GyroZ;
float GyroErrorX, GyroErrorY, GyroErrorZ;
float Groll, Gpitch, Gyaw;
int c = 0;

/* An Mpu9250 object with the MPU-9250 sensor on I2C bus 0 with address 0x68 */
Mpu9250 imu(&Wire, 0x68);
int status;

void setup() {
  /* Serial to display data */
  Serial.begin(19200);

  while (!Serial) {}
  /* Start communication */
  if (!imu.Begin()) {
    Serial.println("IMU initialization unsuccessful");
    while (1) {}
  }
  calculate_IMU_error();
  delay(20);
}

void loop() {
}
void calculate_IMU_error() {
 if (imu.Read()) {
  c = 0;
  // Read gyro values 200 times
  while (c < 200) {

    GyroX = imu.gyro_x_radps() * (180 / PI); // deg/s
    GyroY = imu.gyro_y_radps() * (180 / PI); // deg/s
    GyroZ = imu.gyro_z_radps() * (180 / PI); // deg/s

    // Sum all readings
    GyroErrorX = GyroErrorX + GyroX;
    GyroErrorY = GyroErrorY + GyroY;
    GyroErrorZ = GyroErrorZ + GyroZ;
    c++;
  }
  //Divide the sum by 200 to get the error value
  GyroErrorX = GyroErrorX / 200;
  GyroErrorY = GyroErrorY / 200;
  GyroErrorZ = GyroErrorZ / 200;

  // Print the error values on the Serial Monitor
  Serial.print("GyroErrorX: ");
  Serial.print(GyroErrorX);
  Serial.print(",");
  Serial.print("GyroErrorY: ");
  Serial.print(GyroErrorY);
  Serial.print(",");
  Serial.print("GyroErrorZ: ");
  Serial.println(GyroErrorZ);
}
}
